<!DOCTYPE html>
<html lang="en">
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your own takeaway menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/heroic-features.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">TakeAway Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inputMenu.php">Add Restaurant
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="search.php">Find Restaurant
                    </a>
                </li>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">


    <?php

        if(empty($_GET["restId"])){
            echo "<h1>Error: Abnormal access!</h1>";
        }else{
            require_once('connection/conn.php');
            mysqli_query($conn, "set character set 'utf8'");//读库 解決中文亂碼問題
            mysqli_query($conn, "set names 'utf8'");//写库 解決中文亂碼問題
            //get restaurant information
            $id = $_GET["restId"];
            $id = preg_replace('/[^\p{L}\p{N}\s]/u', "", $id);
            $sql = "SELECT * FROM restaurantdetail WHERE ID='".$id."'";
            $result = $conn->query($sql);

            if($result->num_rows > 0){
                while($row = $result->fetch_assoc()){
                    echo "<h1>You are going to update: ".$row["name"]."</h1>";
                    ?>
                        <form class="form-horizontal" action="update-handle.php" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <input type="hidden" id="restId" name="restId" value="<?php echo $id; ?>">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Restaurant Name</label>
                                    <div class="col-md-4">
                                        <input id="rName" name="rName" type="text" placeholder="Restaurant Name"
                                               class="form-control input-md" required="required"
                                               value="<?php
                                                    echo $row["name"];
                                               ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Type of restaurant</label>
                                    <div class="col-md-4">
                                        <input id="rType" name="rType" type="text" placeholder="Type of restaurant"
                                               class="form-control input-md" required="required"
                                               value="<?php
                                                    echo $row["type"];
                                               ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="address">Address</label>
                                    <div class="col-md-7">
                                        <input id="rAddress" name="rAddress" type="text" placeholder="Restaurant address"
                                               class="form-control input-md" required="required"
                                               value="<?php
                                                    echo $row["location"];
                                               ?>">
                                    </div>
                                </div>

                                <script>
                                    var placeSearch, autocomplete;
                                    var temLat=0 , temLng=0;
                                    var input = document.getElementById('rAddress');

                                    function initAutocomplete() {
                                        autocomplete = new google.maps.places.Autocomplete(input);
                                        autocomplete.addListener('place_changed', fillInAddress);
                                    }

                                    function fillInAddress() {
                                        // Get the place details from the autocomplete object.
                                        var place = autocomplete.getPlace();
                                        temLat = place.geometry.location.lat();
                                        temLng = place.geometry.location.lng();
                                        document.getElementById('lat').value = temLat;
                                        document.getElementById('lng').value = temLng;
                                    }
                                </script>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="address1">Open and Close time</label>
                                    <div class="col-md-4">

                                        <input id="oTime" name="roTime" type="time" placeholder="Open time" class="form-control input-md" required="" 
                                            value="<?php
                                                echo date("h:i", strtotime($row["open"]));
                                            ?>">
                                        <span class="help-block"><center>to</center></span>
                                        <input id="cTime" name="rcTime" type="time" placeholder="Close time" class="form-control input-md" required="" 
                                            value="<?php
                                                echo date("h:i", strtotime($row["close"]));
                                            ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="phone">Phone Number</label>
                                    <div class="col-md-4">
                                        <input id="rPhone" name="rPhone" type="text" placeholder="Restaurant Phone Number" class="form-control input-md" required="" 
                                            value="<?php
                                                echo $row["phone"];
                                            ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="price">Average price</label>
                                    <div class="col-md-4">
                                        <input id="rPrice" name="rPrice" type="number" placeholder="Restaurant Average price" class="form-control input-md" required="" 
                                            value="<?php
                                                echo $row["price"];
                                            ?>">
                                    </div>
                                </div>

                                                                <!--image part-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="img">Add New Menus:</label>
                                    <div class="col-md-4">
                                        <input type="file" id="rImg" name="rImg[]" placeholder="Upload the image file"
                                            class="form-control input-md" multiple>
                                    </div>
                                </div>

                                <!--remove menu-->
                                <div class="form-group" align="center" style="background-color:lightgrey">
                                    <label class="col-md-4 control-label" for="removeMenu"><b style='font-size:24px'>Remove Existing Menu</b></label>
                                    <div>
                                        <?php
                                            //query menu image location from database
                                            $sql = "SELECT * FROM resourseimg WHERE restaurantID='".$id."'";
                                            $imgResult = $conn->query($sql);

                                            if($imgResult->num_rows > 0){
                                                echo "<b style='background-color:yellow'>WARNING</b> : by checking the checkbox below the menu, the menu will be deleted.";
                                                while($imgRow = $imgResult->fetch_assoc()){
                                                    //show image for reference to let user see needed to delete it
                                                    echo "<img src='".$imgRow["image"]."' width='80%'>";
                                                    echo "<br /><br />";
                                                    echo "<input type=\"checkbox\" id=\"removeMenu\" name=\"removeMenu[]\" value=\"".$imgRow["ID"]."\" class=\"form-control input-md\">";
                                                    echo "<hr />";
                                                }
                                            }else{
                                                echo "<b>This restaurant has no menus!</b>";
                                            }
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group" align="center" style="background-color:yellow">
                                    <label class="col-md-4 control-label" for="isWorking"><b style='font-size:24px'>Is the restaurant working?</b></label>
                                    <div>
                                        <input type="checkbox" id="isWorking" name="isWorking" value="1" class="form-control input-md" 
                                        <?php
                                            if($row["isWorking"]){
                                                echo "checked";
                                            }
                                        ?>>
                                        <b style='background-color:red; color:white'>WARNING</b> : If you uncheck the box and update, this restaurant will no longer be found by "search" function)
                                    </div>
                                </div>

                                <input type="hidden" id="lat" name="lat" value="<?php echo $row["LocationX"]; ?>">
                                <input type="hidden" id="lng" name="lng" value="<?php echo $row["LocationY"]; ?>">
                                <hr />
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="save"></label>
                                    <div class="col-md-8">
                                        <input type="submit" id="save" name="save" class="btn btn-success" value="Update">
                                        <a href="restaurant.php?restId=<?php echo $id;?>">
                                            <button type="button" class="btn btn-danger">Cancel update</button>
                                        </a>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    <?php
                }
            }else{
                echo "<h1 class=\"display-3\" align=\"center\">Restaurant id invalid! No records found!</h1>";
            }           
            $conn->close();
        }
    ?>

    
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_jtoEQdm4NtyokURH_LO-_f2wUYJTEKs&libraries=places&callback=initAutocomplete&language=en"></script>
</body>

</html>
