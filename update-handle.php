<?php

if (empty($_POST["rName"]) || empty($_POST["restId"])) {
    echo "Abnormal access!";
} else {
    $id = $_POST['restId'];
    $Name = $_POST['rName'];
    $Location = $_POST['rAddress'];
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $Type = $_POST['rType'];
    $Phone = $_POST['rPhone'];
    $Open = $_POST['roTime'];
    $Close = $_POST['rcTime'];
    $Price = $_POST['rPrice'];
    $isWorking = isset($_POST['isWorking']); //bool, 0 or 1
    $target_dir = "uploads/";
    $uploadOk = 1;

    $arr = array();
    $count = 0;

    //----------------------
    // Check if image file is a actual image or fake image
    if (isset($_POST["save"])) {
        if ($lat == null) {
            $lat = 0;
            $lng = 0;
        }

        //start working on db
        require_once('connection/conn.php');
        //update non image values
        if ($conn->connect_error) {
        }
        mysqli_query($conn, "set character set 'utf8'");//读库 解決中文亂碼問題
        mysqli_query($conn, "set names 'utf8'");//写库 解決中文亂碼問題
        $sql = "UPDATE restaurantdetail
                SET name='" . $Name . "', location='" . $Location . "', LocationX='" . $lat . "', LocationY='" . $lng . "', type='" . $Type . "', phone='" . $Phone . "', open='" . $Open . "', close='" . $Close . "', price='" . $Price . "', isWorking='" . $isWorking . "' 
                WHERE ID='" . $id . "'";
        if ($conn->query($sql)) {
            echo "update success";
        } else {
            echo "update fail";
        }
    }

    //check error, if error code=0 then is uploaded
    foreach ($_FILES["rImg"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {

            $tmp_name = $_FILES["rImg"]["tmp_name"][$key];
            $a = explode(".", $_FILES["rImg"]["name"][$key]); //截取文件名跟后缀
            // $prename = substr($a[0],10); //如果你到底的图片名称不是你所要的你可以用截取字符得到
            $prename = $a[0];
            $name = date('YmdHis') . mt_rand(100, 999) . "." . $a[1]; // 文件的重命名 （日期+随机数+后缀）
            $uploadfile = $target_dir . $name; // 文件的路径
            $imageFileType = strtolower(pathinfo($uploadfile, PATHINFO_EXTENSION));

            $check = getimagesize($_FILES["rImg"]["tmp_name"][$key]);

            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
            if (file_exists($uploadfile)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }

            // Check file size
            if ($_FILES["rImg"]["size"][$key] > 3000000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    echo "The file " . $uploadfile . " has been uploaded.";//$uploadfile是圖片的路徑
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            $arr[$count] = $uploadfile;

            $sql2 = "INSERT INTO resourseimg (restaurantID, image) VALUES ('" . $id . "','" . $uploadfile . "')";
            mysqli_query($conn, $sql2);
            $count++;
        }
    }

    //remove menu if selected
    if (empty($_POST["removeMenu"])) {
        //echo "no menu to be remove";
    } else {
        $removeMenu = $_POST["removeMenu"];
        //echo "some menu selected";
        //echo "count: ".count($removeMenu);
        //---delete selected menu
        foreach ($removeMenu as $value) {
            echo "<br />";
            echo "curMenu value: " . $value;
            echo "<br />";
            $sql = "DELETE FROM resourseimg
                    WHERE ID='" . $value . "'";
            echo $sql;
            if ($conn->query($sql)) {
                echo "<br />";
                echo "menu with ID:" . $value . " deleted";
            } else {
                echo "error" . $conn->error;
            }
        }
    }
    $conn->close();
}


echo "<br />";
echo "You will be redirected to the corresponding restaurant information page in few seconds.";
header("refresh:3;restaurant.php?restId=" . $id);
//header('Location: restaurant.php?restId='.$id);
?>