<!DOCTYPE html>
<html lang="en">
<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your own takeaway menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/heroic-features.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">TakeAway Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="inputMenu.php">Add Restaurant
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="search.php">Find Restaurant
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <!-- search options -->
    <div class="container">

      <h1 class="display-3" align="center">Find Restaurant Now!</h1>
    
      <!-- Upper(search options) -->
      <form class="form-horizontal" action="/search.php" method="get" enctype="multipart/form-data">
        <fieldset>
          <div class="form-group">
              <div class="col-lg-12">
                <input type="text" id="searchKey" name="searchKey" placeholder="Restaurant name, location..."
                    class="form-control input-md" required="required"
                    value="<?php
                      if(!empty($_GET["searchKey"])){
                        $searchKeys = explode(" ", $_GET['searchKey']);
                        $searchKeys = preg_replace('/[^\p{L}\p{N}\s]/u', "", $searchKeys);
                        $keyLength = count($searchKeys);
                        foreach ($searchKeys as $val){
                          if($val == ""){
                            $keyLength --;
                          }
                        }
                        if($keyLength >= 1){
                          $i = 0;
                          foreach ($searchKeys as $val){
                            if($val != ""){
                              echo $val." ";
                            }
                          }
                        }
                      }
                    ?>">
              </div>
          </div>

          <div class="form-group">
              <div align="center">
                <input type="submit" id="search" name="search" class="btn btn-success" value="Search">
              </div>
          </div>

        </fieldset>
      </form>
    </div>
    
    <hr />

    <!-- Lower(search results) -->

    <?php
      //if searchKey is null
      if(empty($_GET['searchKey'])){
        echo "<div class=\"jumbotron\">";
        echo "<h2>No detail for searching yet! Please input from above!</h2>";
        echo "</div>";
      }else{
        //---check if searchKey is valid
        //split keys by space
        $searchKeys = explode(" ", $_GET['searchKey']);
        //remove symbols
        $searchKeys = preg_replace('/[^\p{L}\p{N}\s]/u', "", $searchKeys);
        $keyLength = count($searchKeys);
        //echo "length: ".$keyLength;
        //---count actual length of keys
        //reason:split by " " but there exist possibility of "  "(multiple spaces) and allow "" to become searchKey 
        foreach ($searchKeys as $val){
          if($val == ""){
            $keyLength --;
          }else{
            //echo "<br />";
            //echo "\"" . $val . "\"";
          }
        }
        //echo "length after: ".$keyLength;

        //---if actual length of key >= 1
        if($keyLength >= 1){
          $i = 0;
          $splitedKeys;
          //take all valid keys(not "") and put into $splitedKeys
          foreach ($searchKeys as $val){
            if($val != ""){
              $splitedKeys[$i] = $val;
              //echo "<br />";
              //echo "\"" . $splitedKeys[$i] . "\"";
              $i++;
            }
          }
          //echo "after array length: ".count($splitedKeys);
          
          //---start working on database
          require_once('connection/conn.php');

          if($conn->connect_error){
            die("CONNECTION FAILED! ". $conn->connect_error);
          }else{
            //echo "<br />";
            $keyLength = count($splitedKeys);
            //---which column should search from database
            $arrCols = array("name", "location", "type");
            $order;
            $restExistCount = 3; //decrease(-1) if no results found by name/location/type
            
            //---col loop, select from database everytime by column of $arrCols
            for ($l = 0; $l < 3; $l++){
              //---loop all keys and create a $sql for query
              $sql = "SELECT ID FROM restaurantdetail".
              " WHERE (".$arrCols[$l]." LIKE '%".$splitedKeys[0]."%'";
              for ($k = 1; $k < $keyLength; $k++){
                $sql = $sql." OR ".$arrCols[$l]." LIKE '%".$splitedKeys[$k]."%'";
              }
              $sql = $sql.") AND isWorking=1";

              //echo "<br />";
              //echo $sql;
              //---query
              $result = $conn->query($sql);

              //---check if result found
              if($result->num_rows > 0){
                //---result loop
                //put distinct result into $order, and check priority
                //the more the records be found from name/location/type get higher priority
                //default priority=0
                while($row = $result->fetch_assoc()){
                  //echo "<br />";
                  //echo "cur loop: ".$l;
                  //echo " id: ".$row["ID"]; //see if there are results
                  if(empty($order)){
                    //assign first record found with 0 pri
                    $order[0] = array($row["ID"], 0);
                  }else{
                    //loop through $order to see if the record already exist
                    //if exists, add priority/if not exists, push into $order
                    for($m = 0; $m < count($order); $m++){
                      if($order[$m][0] == $row["ID"]){
                        $order[$m][1]++;
                        break;
                      }else if($m+1 == count($order)){
                        $order[$m+1] = array($row["ID"], 0);
                        break;
                      }
                    }
                  }
                }
              }else{
                //if no result found, eg. where name='kfc' return 0results, then decrease(original 3)
                //if name, location, type both return 0 results, then it become 0, which means nothing found
                $restExistCount--;
              }
            }

            //---if there are records, keep working
            if($restExistCount > 0){
                $max = 0;
                //---get max value of priority for all orders
                foreach($order as $curOrder){
                  //echo "orderS: ".$curOrder[0]." orderP: ".$curOrder[1];
                  //echo "<br />";
                  if($curOrder[1] > $max){
                    $max = $curOrder[1];
                  }
                }
                //echo "max: ".$max;

                $sortedOrders;
                $curPlace = 0;
                //---order id by priority, make use of $max
                for($curMax = $max; $curMax >= 0; $curMax--){
                  for($n = 0; $n < count($order); $n++){
                    if($order[$n][1] == $curMax){
                      $sortedOrders[$curPlace++] = $order[$n][0];
                    }
                  }
                }

                //echo "<br />";
                //echo "sorted order:";
                //echo count($sortedOrders);

                //---create $sql string for query all required data
                $strIds = " ID='".$sortedOrders[0]."' ";
                for ($o = 1; $o < count($sortedOrders); $o++){
                  $strIds = $strIds." OR ID='".$sortedOrders[$o]."'";
                }
                $sql = "SELECT * FROM restaurantdetail".
                " WHERE ".$strIds."";
                //echo "<br />";
                //echo $sql;

                //---qurey result
                $result = $conn->query($sql);
                
                $curPlace = 0;
                $orderedResult;
                //sort the results by priority into $orderedResult
                if($result->num_rows > 0){
                  $resultCount = $result->num_rows;
                  while($row = $result->fetch_assoc()){
                      for($p = 0; $p < $resultCount; $p++){
                        //echo "<br />";
                        //echo " ".$sortedOrders[$p];
                        if($sortedOrders[$p] == $row["ID"]){
                          //put into corresponding position
                          $orderedResult[$p] = $row;
                          //echo "curNum ".$sortedOrders[$p]."|to :".$p."| gocount ".count($orderedResult);
                          break;
                        }
                      }
                    }
                  }

                  //print result by order
                  for($q = 0; $q < count($orderedResult); $q++){
                    $href = "restaurant.php?restId=".$orderedResult[$q]["ID"];
                    echo "<a href=\"".$href."\">";
                      echo "<div class=\"jumbotron\">";
                        echo "<div class=\"container\">";
                          echo "<h2>".$orderedResult[$q]["name"]." (".$orderedResult[$q]["type"].")</h2>";
                        echo "</div>";
                        echo "<div class=\"container\">";
                          echo $orderedResult[$q]["location"];
                        echo "</div>";
                        echo "<br />";
                        echo "<div class=\"container\">";
                          echo "<table>";
                            echo "<tr>";
                              echo "<th>Price:</th>";
                              echo "<td>  ~$".$orderedResult[$q]["price"]."</td>";
                            echo "</tr>";
                            echo "<tr>";
                              echo "<th>Opening Hours:</th>";
                              echo "<td>  ".date("h:ia", strtotime($orderedResult[$q]["open"]))." - ".date("h:ia", strtotime($orderedResult[$q]["close"]))."</td>";
                            echo "</tr>";
                            echo "<tr>";
                              echo "<th>Tel:</th>";
                              echo "<td>  ".$orderedResult[$q]["phone"]."</td>";
                            echo "</tr>";
                          echo "</table>";
                        echo "</div>";
                      echo "</div>";
                    echo "</a>";
                  }
                }else{ //no records, give out message
                  echo "<div class=\"jumbotron\">";
                  echo "<h2>Sorry! No restaurants found!</h2>";
                  echo "</div>";
                }
                
            $conn->close();
          }
        }else{ //all keywords are spaces
          echo "keywords are all spaces!";
        }
      }
    ?>


    <!-- Bootstrap core JavaScript -->
    

  </body>

</html>
