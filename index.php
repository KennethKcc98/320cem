<!DOCTYPE html>
<html lang="en">
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your own takeaway menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/heroic-features.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">TakeAway Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inputMenu.php">Add Restaurant
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="search.php">Find Restaurant
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Jumbotron Header -->
    <header class="jumbotron my-4">
        <h1 class="display-3">Your own takeaway menu</h1>
    </header>

    <!-- Page Features -->
    <?php
    //create card samples
    require_once('connection/conn.php');
    if ($conn->connect_error) {
        die("CONNECTION FAILED! " . $conn->connect_error);
    } else {
        mysqli_query($conn, "set character set 'utf8'");//读库 解決中文亂碼問題
        mysqli_query($conn, "set names 'utf8'");//写库 解決中文亂碼問題
        $sql = "SELECT * FROM restaurantdetail WHERE isWorking=1 LIMIT 4";
        $result = $conn->query($sql);
        //echo "selected records: ".$result->num_rows;
        if ($result->num_rows > 0) {
            echo "<div class=\"row text-center\">";
            while ($row = $result->fetch_assoc()) {
                //query image for restaurant
                $imgSql = "SELECT * FROM resourseimg 
              WHERE restaurantID='" . $row["ID"] . "' LIMIT 1";
                $imgResult = $conn->query($imgSql);
                $imgSrc = "http://placehold.it/500x325";
                if ($imgResult->num_rows > 0) {
                    while ($imgRow = $imgResult->fetch_assoc()) {
                        $imgSrc = $imgRow["image"];
                    }
                }

                //print card
                ?>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card">
                        <img class="card-img-top" src="<?php echo $imgSrc; ?>" alt="">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo $row["name"]; ?></h4>
                            <p class="card-text"><?php echo $row["type"]; ?></p>
                            <p class="card-text"><?php echo $row["location"]; ?></p>
                        </div>
                        <div class="card-footer">
                            <a href="restaurant.php?restId=<?php echo $row["ID"]; ?>" class="btn btn-primary">Find Out
                                More!</a>
                        </div>
                    </div>
                </div>

                <?php
            }
            echo "</div>";
        }
    }
    ?>

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript -->


</body>

</html>
