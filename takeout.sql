-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 20, 2018 at 11:04 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `takeout`
--

-- --------------------------------------------------------

--
-- Table structure for table `resourseimg`
--

DROP TABLE IF EXISTS `resourseimg`;
CREATE TABLE IF NOT EXISTS `resourseimg` (
  `ID` int(12) NOT NULL AUTO_INCREMENT,
  `restaurantID` int(11) NOT NULL,
  `image` varchar(55) NOT NULL,
  `meaning` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resourseimg`
--

INSERT INTO `resourseimg` (`ID`, `restaurantID`, `image`, `meaning`) VALUES
(23, 19, 'uploads/20181220110317575.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `restaurantdetail`
--

DROP TABLE IF EXISTS `restaurantdetail`;
CREATE TABLE IF NOT EXISTS `restaurantdetail` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,

  `name` varchar(50) COLLATE utf8_unicode_ci,
  `location` varchar(250) COLLATE utf8_unicode_ci NOT NULL,

  `LocationX` double(10,6) DEFAULT NULL,
  `LocationY` double(10,6) DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(20) DEFAULT NULL,
  `open` time(6) DEFAULT NULL,
  `close` time(6) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `edittime` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6),
  `isWorking` bool NOT NULL DEFAULT 1,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `restaurantdetail`
--

INSERT INTO `restaurantdetail` (`ID`, `name`, `location`, `LocationX`, `LocationY`, `type`, `phone`, `open`, `close`, `price`, `edittime`) VALUES

(19, 'mcdonalds', 'Hong Kong, fotan', 22.398571, 114.192943, 'fast food', 11123231, '01:00:00.000000', '12:59:00.000000', 31.00, '2018-12-20 11:03:17.038780');


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `uID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  `password` varchar(30) NOT NULL,
  `text` varchar(30) NOT NULL,
  PRIMARY KEY (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
